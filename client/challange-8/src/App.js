
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import NewUserInput from './components/NewUserInput';
import Header from './templates/Header';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import EditUserInput from './components/EditUserInput';
import ListPlayer from './templates/ListPlayer';
import { Container } from 'react-bootstrap';
import SearchPlayer from './components/SearchPlayer';

function App() {
  return (
    <div>
      <Header />
      <Router>
        <Switch>
          <Container>
            <Route path="/" exact>
              <ListPlayer />
            </Route>
            <Route path="/player">
              <NewUserInput />
            </Route>
            <Route path="/edit" exact>
              <EditUserInput />
            </Route>
            <Route path="/search" exact>
              <SearchPlayer />
            </Route>
          </Container>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
