import React, { Fragment, useState } from 'react'
import { useParams } from 'react-router-dom';
import { Form, Container, Button } from 'react-bootstrap'
function EditUserInput() {
    const [username, setUsername] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [experience, setExperience] = useState('')
    const [lvl, setLvl] = useState('')

    const usernameSubmit = (e) => {
        let data = e.target.value
        console.log(data);
    }
    const emailSubmit = (e) => {
        let data = e.target.value
        console.log(data);
    }
    const passwordSubmit = (e) => {
        let data = e.target.value
        console.log(data);
    }
    const experienceSubmit = (e) => {
        let data = e.target.value
        console.log(data);
    }
    const lvlSubmit = (e) => {
        let data = e.target.value
        console.log(data);
    }
    const submitData = () => {
        let data = {
            username: username,
            email: email,
            password: password,
            experience: parseInt(experience),
            lvl: parseInt(lvl)
        }
    }
    return (
        <div>
            <Fragment>
                <h3>EDIT PLAYER</h3>
                <Form>
                    <Form.Group controlId="exampleForm.ControlInput1">
                        <Form.Label>Username</Form.Label>
                        <Form.Control type="text" placeholder="Username" value={username} onChange={usernameSubmit} />
                    </Form.Group>
                    <Form.Group controlId="exampleForm.ControlInput2">
                        <Form.Label>Email</Form.Label>
                        <Form.Control type="email" placeholder="name@example.com" />
                    </Form.Group>
                    <Form.Group controlId="exampleForm.ControlInput2">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" value={password} onChange={passwordSubmit} />
                    </Form.Group>
                    <Form.Group controlId="exampleForm.ControlInput2">
                        <Form.Label>Experience</Form.Label>
                        <Form.Control type="number" value={experience} onChange={experienceSubmit} />
                    </Form.Group>
                    <Form.Group controlId="exampleForm.ControlInput2">
                        <Form.Label>Level</Form.Label>
                        <Form.Control type="number" value={lvl} onChange={lvlSubmit} />
                    </Form.Group>
                    <Form.Group controlId="exampleForm.ControlInput2">
                        <Button variant="primary" size="lg" block onClick={submitData}>
                            Submit
                    </Button>
                    </Form.Group>
                </Form>
            </Fragment>
        </div>
    )
}

export default EditUserInput
