import React, { Fragment, useState } from 'react'
import { Form, Button } from 'react-bootstrap'
function NewUserInput() {
    const [username, setUsername] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [experience, setExperience] = useState('')
    const [lvl, setLvl] = useState('')

    const usernameSubmit = (e) => {
        let data = e.target.value
        setUsername(data);
    }
    const emailSubmit = (e) => {
        let data = e.target.value
        setEmail(data);
    }
    const passwordSubmit = (e) => {
        let data = e.target.value
        setPassword(data);
    }
    const experienceSubmit = (e) => {
        let data = e.target.value
        setExperience(parseInt(data));
    }
    const lvlSubmit = (e) => {
        let data = e.target.value
        setLvl(parseInt(data));
    }
    const submitData = async () => {
        let data = {
            username: username,
            email: email,
            password: password,
            exp: experience,
            lvl: lvl
        }
        const response = await fetch("http://localhost:5000/api/players", {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(data)
        })
        window.location = "/";
        // console.log(data);
    }
    return (
        <Fragment>
            <h3>CREATE NEW PLAYER</h3>
            <Form>
                <Form.Group controlId="exampleForm.ControlInput1">
                    <Form.Label>Username</Form.Label>
                    <Form.Control type="text" placeholder="Username" value={username} onChange={usernameSubmit} />
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlInput2">
                    <Form.Label>Email</Form.Label>
                    <Form.Control type="email" placeholder="name@example.com" value={email} onChange={emailSubmit} />
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlInput2">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" value={password} onChange={passwordSubmit} />
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlInput2">
                    <Form.Label>Experience</Form.Label>
                    <Form.Control type="number" value={experience} onChange={experienceSubmit} />
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlInput2">
                    <Form.Label>Level</Form.Label>
                    <Form.Control type="number" value={lvl} onChange={lvlSubmit} />
                </Form.Group>
                <Form.Group controlId="exampleForm.ControlInput2">
                    <Button variant="primary" size="lg" block onClick={submitData}>
                        Submit
                    </Button>
                </Form.Group>
            </Form>
        </Fragment>
    )
}

export default NewUserInput
