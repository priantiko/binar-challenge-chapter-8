import React, { useState, useEffect, Fragment } from 'react'
import { Row, Col, InputGroup, FormControl, Table, Button } from 'react-bootstrap'

function SearchPlayer() {
    const [players, setPlayers] = useState([])
    const [search, setSearch] = useState([])
    const [searchbyemail, setSearchByEmail] = useState([])
    const [searchbyexp, setSearchByExp] = useState([])
    const [searchbylvl, setSearchByLvl] = useState([])

    const ListPlayer = async () => {
        const response = await fetch('http://localhost:5000/api/players')
        const data = await response.json()
        setPlayers(data.message);
    }

    const usernameSubmit = (e) => {
        let data = e.target.value
        setSearch(data)
    }
    const emailSubmit = (e) => {
        let data = e.target.value
        setSearchByEmail(data)
    }
    const expSubmit = (e) => {
        let data = e.target.value
        setSearchByExp(data)
    }
    const lvlSubmit = (e) => {
        let data = e.target.value
        setSearchByLvl(data)
    }
    console.log(search);
    console.log(searchbyemail);
    const FilteringByPlayer = players.filter(player => {
        // if (search !== "") {
        //     return player.username.includes(search)
        // }
        return player.username.includes(search)
        // if (searchbyemail !== "") {
        //     return player.email.includes(searchbyemail)
        // }
        // if (searchbyexp !== "") {
        //     return player.experience.includes(searchbyexp)
        // }
        // if (searchbylvl !== "") {
        //     return player.lvl.includes(searchbylvl)
        // }
    })

    // const emailSubmit = (e) => {
    //     let data = e.target.value
    //     setEmail(data);
    // }
    // const experienceSubmit = (e) => {
    //     let data = e.target.value
    //     setExperience(parseInt(data));
    // }
    // const lvlSubmit = (e) => {
    //     let data = e.target.value
    //     setLvl(parseInt(data));
    // }
    // const onSubmit = () => {

    // }
    useEffect(() => {
        ListPlayer()
    }, [])

    return (
        <Fragment>
            <Row className="mb-5">
                <Col>
                    <InputGroup className="mb-3">
                        <FormControl
                            placeholder="Username"
                            aria-label="Username"
                            aria-describedby="basic-addon1"
                            onChange={usernameSubmit}
                        />
                    </InputGroup>
                </Col>
                <Col>
                    <InputGroup className="mb-3">
                        <FormControl
                            placeholder="Email"
                            aria-label="Email"
                            aria-describedby="basic-addon1"
                            onChange={emailSubmit}
                        />
                    </InputGroup>
                </Col>
                <Col>
                    <InputGroup className="mb-3">
                        <FormControl
                            placeholder="Experience"
                            aria-label="Experience"
                            aria-describedby="basic-addon1"
                            onChange={expSubmit}
                        />
                    </InputGroup>
                </Col>
                <Col>
                    <InputGroup className="mb-3">
                        <FormControl
                            placeholder="Level"
                            aria-label="Level"
                            aria-describedby="basic-addon1"
                            onChange={lvlSubmit}
                        />
                    </InputGroup>
                </Col>
                <Button variant="primary" size="lg" block>
                    Submit
                    </Button>
            </Row>

            <h3>FIND PLAYER</h3>
            <Table striped bordered hover variant="dark">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Experience</th>
                        <th>level</th>

                    </tr>
                </thead>
                <tbody>
                    {
                        FilteringByPlayer.map(
                            player => (<tr key={player.id}>
                                <td>{player.id}</td>
                                <td>{player.username}</td>
                                <td>{player.email}</td>
                                <td>{player.experience}</td>
                                <td>{player.lvl}</td>

                            </tr>
                            )
                        )
                    }

                </tbody>
            </Table>
        </Fragment>
    )
}

export default SearchPlayer
