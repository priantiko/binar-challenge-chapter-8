import React from 'react'
import '../App.css'
import LogoBinar from '../binar-logo.png'
import { Navbar, Button, Nav, FormControl } from 'react-bootstrap'
import {
    BrowserRouter as Router,
} from "react-router-dom";
function Header() {
    return (
        <>
            <Router>
                <Navbar variant="dark" className="mb-5 binar-color-background">
                    <Navbar.Brand href="/">
                        <img
                            src={LogoBinar}
                            width="100"
                            height="50"
                            className="d-inline-block align-top"
                            alt=""
                        />
                    </Navbar.Brand>
                    <Nav className="mr-auto">
                        <Nav.Link href="/player">Player</Nav.Link>
                        <Nav.Link href="/edit">Edit</Nav.Link>
                        <Nav.Link href="/search">Search</Nav.Link>
                    </Nav>

                </Navbar>
            </Router>
        </>
    )
}

export default Header
