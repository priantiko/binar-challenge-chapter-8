import React, { useState, useEffect, Fragment } from 'react'
import { Table, Button } from 'react-bootstrap'

function ListPlayer() {
    const [players, setPlayers] = useState([])

    const ListPlayer = async () => {
        const response = await fetch('http://localhost:5000/api/players')
        const data = await response.json()
        setPlayers(data.message);
    }
    const deletePlayer = async id => {
        try {
            const response = await fetch(`http://localhost:5000/api/players/${id}`, {
                method: "DELETE"
            });

            setPlayers(players.filter(player => player.id !== id));
        } catch (err) {
            console.error(err.message);
        }
    };
    useEffect(() => {
        ListPlayer()
    }, [])
    console.log(players);
    return (
        <Fragment>
            <h3>LIST PLAYERS</h3>
            <Table striped bordered hover variant="dark">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Experience</th>
                        <th>level</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        players.map(
                            player => (<tr key={player.id}>
                                <td>{player.id}</td>
                                <td>{player.username}</td>
                                <td>{player.email}</td>
                                <td>{player.experience}</td>
                                <td>{player.lvl}</td>
                                <td><Button onClick={() => deletePlayer(player.id)} variant="danger">Delete</Button></td>
                            </tr>
                            )
                        )
                    }

                </tbody>
            </Table>
        </Fragment>
    )
}

export default ListPlayer
